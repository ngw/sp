require 'optparse'

require_relative 'smart_pension/cli'
require_relative 'smart_pension/log_parser'
require_relative 'smart_pension/sorting_service'
require_relative 'smart_pension/presenter'

module SmartPension
  VERSION = '0.1'
end
