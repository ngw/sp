module SmartPension
  class LogParser
    attr_reader :page_views

    def initialize(file:)
      @file = open(file, 'r')
      # Pure awesomeness
      @page_views = Hash.new { |h, key| h[key] = Hash.new(0) }
      parse!
    rescue Errno::ENOENT
      puts "File #{file} doesn't exist"
      raise
    rescue Errno::EACCES
      puts "File #{file} can't be read"
      raise
    end

    private

    def parse!
      @file.each do |line|
        page_url, ip = line.split
        @page_views[page_url][ip] += 1
      end
    end
  end
end
