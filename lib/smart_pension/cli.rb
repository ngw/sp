module SmartPension
  class CLI
    def initialize
      @options = option_parse
    end

    def perform
      log_parser = LogParser.new(file: @options[:file])
      sorting = SortingService.new(page_views: log_parser.page_views)
      Presenter.new(by_number_of_views: sorting.by_number_of_views,
                    by_unique_visitors: sorting.by_unique_visitors)
    end

    private

    def option_parse
      options = {}
      OptionParser.new do |opts|
        opts.banner = "Usage: smart_pension [options]"
        opts.on("-f", "--file FILE", String, "Parse logfile FILE") do |file|
          options[:file] = file
        end
      end.parse!
      raise OptionParser::MissingArgument.new("You must provide a --file") if options.empty?
      options
    end
  end
end
