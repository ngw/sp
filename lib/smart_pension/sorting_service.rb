module SmartPension
  class SortingService
    def initialize(page_views:)
      @page_views = page_views
    end

    def by_number_of_views
      views_sum = @page_views.map { |k, v| [ k, v.values.sum ] }
      sort(views_sum)
    end

    def by_unique_visitors
      views_uniques = @page_views.map { |k, v| [ k, v.keys.length ] }
      sort(views_uniques)
    end

    private

    def sort(tuples)
      tuples.sort_by { |k, v| v }.reverse
    end
  end
end
