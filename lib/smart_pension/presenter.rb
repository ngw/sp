module SmartPension
  class Presenter
    def initialize(by_number_of_views:, by_unique_visitors:)
      @by_number_of_views = by_number_of_views
      @by_unique_visitors = by_unique_visitors
      puts text_to_display
    end

    private

    def text_to_display
      ( map(views: @by_number_of_views, bullet: "*", object: "views") +
        ["*-*-*-*-+-+-+-*-*-*-*-*"] +
        map(views: @by_unique_visitors, bullet: "+", object: "uniques")
      ).join("\n")
    end

    def map(views:, bullet:, object:)
      views.collect do |page|
        "#{bullet} #{page[0]} had #{page[1]} #{object}"
      end
    end
  end
end
