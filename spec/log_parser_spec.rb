RSpec.describe SmartPension::LogParser do
  context 'parse!' do
    let(:log_parser) do
      described_class.new(file: 'spec/fixtures/webserver.log')
    end

    it "parses all visits" do
      expect(log_parser.page_views).to eq(
        { "/help_page/1"=>{"126.318.035.038"=>1, "929.398.951.889"=>1, "646.865.545.408"=>1}, 
          "/contact"=>{"184.123.665.067"=>1}, 
          "/home"=>{"184.123.665.067"=>1}, 
          "/about/2"=>{"444.701.448.104"=>1, "543.910.244.929"=>1}, 
          "/index"=>{"184.123.665.067"=>1, "451.106.204.921"=>1, "127.0.0.1"=>2}} )
    end

    it "increments the visit counter for multiple visits" do
      expect(log_parser.page_views["/index"]["127.0.0.1"]).to eq(2)
    end

    it "counts all visits" do
      expect(log_parser.page_views["/help_page/1"].count).to eq(3) 
    end
  end
end
