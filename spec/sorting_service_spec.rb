RSpec.describe SmartPension::SortingService do
  let(:page_views) do
     { "/help_page/1"=>{"126.318.035.038"=>1, "929.398.951.889"=>1, "646.865.545.408"=>1}, 
       "/contact"=>{"184.123.665.067"=>1}, 
       "/home"=>{"184.123.665.067"=>1}, 
       "/about/2"=>{"444.701.448.104"=>1, "543.910.244.929"=>1}, 
       "/index"=>{"184.123.665.067"=>1, "451.106.204.921"=>1, "127.0.0.1"=>2} } 
  end
  let(:sorting_service) { described_class.new(page_views: page_views) }

  context 'by_number_of_views' do
    it 'sorts views properly' do
      expect(sorting_service.by_number_of_views).to eq(
        [ ["/index", 4], 
          ["/help_page/1", 3], 
          ["/about/2", 2], 
          ["/home", 1], 
          ["/contact", 1] ] )
    end
  end

  context 'by_unique_visitors' do
    it 'sorts views properly' do
      expect(sorting_service.by_unique_visitors).to eq(
        [ ["/index", 3], 
          ["/help_page/1", 3], 
          ["/about/2", 2], 
          ["/home", 1], 
          ["/contact", 1] ] )
    end
  end
end
