RSpec.describe SmartPension::CLI do
  context 'option_parse' do
    it 'parses --file FILE properly' do
      stub_const('ARGV', %w(--file /whatever.log))
      expect { described_class.new.perform }.to raise_error(Errno::ENOENT)
    end

    it 'expects an argument' do
      expect { described_class.new }.to raise_error(OptionParser::MissingArgument)
    end
  end
end
