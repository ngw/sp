RSpec.describe SmartPension::Presenter do
  context 'new' do
    let(:page_views) do
      { "/help_page/1"=>{"126.318.035.038"=>1, "929.398.951.889"=>1, "646.865.545.408"=>1}, 
        "/contact"=>{"184.123.665.067"=>1}, 
        "/home"=>{"184.123.665.067"=>1}, 
        "/about/2"=>{"444.701.448.104"=>1, "543.910.244.929"=>1}, 
        "/index"=>{"184.123.665.067"=>1, "451.106.204.921"=>1, "127.0.0.1"=>2} } 
    end
    let(:sorting_service) { SmartPension::SortingService.new(page_views: page_views) }

    it "displays data nicely" do
      expect {
        described_class.new(
          by_number_of_views: sorting_service.by_number_of_views,
          by_unique_visitors: sorting_service.by_unique_visitors)
      }.to output(
        <<~END
          * /index had 4 views
          * /help_page/1 had 3 views
          * /about/2 had 2 views
          * /home had 1 views
          * /contact had 1 views
          *-*-*-*-+-+-+-*-*-*-*-*
          + /index had 3 uniques
          + /help_page/1 had 3 uniques
          + /about/2 had 2 uniques
          + /home had 1 uniques
          + /contact had 1 uniques
        END
      ).to_stdout
    end
  end
end
