# Smart Pension Log reader

```
bin/smart_pension --file assets/webserver.log
```

## NOTES

I prefer explicitely raising exceptions when file is not readable/unavailable as there's no possible way the program can run successfully, so crashing looks like a sensible choice.

